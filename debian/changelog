festvox-ru (0.5+dfsg-7) UNRELEASED; urgency=medium

  * control: Bump Standards-Version to 4.6.0 (no change)
  * control: Make Multi-Arch: foreign.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 08 Jan 2022 17:06:28 +0100

festvox-ru (0.5+dfsg-6) unstable; urgency=medium

  [ Samuel Thibault ]
  * watch: Generalize pattern.
  * control: Update alioth list domain.
  * TODO.Debian: Rename to TODO so dh_installdocs can find it.
  * control: Bump Standards-Version to 4.5.0 (no change)
  * control: Update salsa URL.
  * control: Set Rules-Requires-Root to no.
  * copyright: Drop closed website URL.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 18 Sep 2021 18:09:54 +0200

festvox-ru (0.5+dfsg-5) unstable; urgency=medium

  * control: Switch maintenance to the tts-team (Closes: #931974)
  * control: Bump Standards-Version to 4.4.0 (no changes).

 -- Samuel Thibault <sthibault@debian.org>  Thu, 29 Aug 2019 01:02:16 +0200

festvox-ru (0.5+dfsg-4) unstable; urgency=medium

  * Update Vcs-* fields for salsa.
  * Point copyright link to SF (berlios is dead)
  * Update debhelper compatibility version
  * Bump up Standards-Version (to 4.1.4)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Fri, 01 Jun 2018 10:57:13 +0300

festvox-ru (0.5+dfsg-3) unstable; urgency=medium

  * Bump up Standards-Version (to 3.9.5)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Sat, 25 Oct 2014 15:54:05 +0400

festvox-ru (0.5+dfsg-2) unstable; urgency=medium

  * Bump up Standards-Version (to 3.9.5)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Sat, 30 Aug 2014 15:57:24 +0400

festvox-ru (0.5+dfsg-1) unstable; urgency=medium

  * Fix lintian error: vcs-field-not-canonical
  * Provide festival-voice virtual package (Closes: #752668)
  * Fix proclaim_voice parameters (Closes: #752673, thanks
    to Dmitry Eremin-Solenikov)
  * Imported Upstream version 0.5+dfsg.  We can't get rid of
    old package build scheme without upstream version change.
    This is it.  Not changes for the original tarball was made.
  * Use debhelper, not cdbs
  * Point watch file and docs to SF page (berlios is dead)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Wed, 23 Jul 2014 15:01:30 +0400

festvox-ru (0.5-6) unstable; urgency=low

  * Drop DMUA, fix lintian warning
  * Bump up Standards-Version (to 3.9.4)

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Thu, 27 Dec 2012 14:12:01 +0400

festvox-ru (0.5-5) unstable; urgency=low

  * Adopt machine-readable debian/copyright file format
  * Bump up Standards-Version to 3.9.3 (no changes)
  * Drop inactive uploaders
  * Add debian/TODO.Debian

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Mon, 05 Mar 2012 16:00:44 +0400

festvox-ru (0.5-4) unstable; urgency=low

  * Added watch file
  * Bump up Standards-Version to 3.9.2.
  * Override lintian info message for "too short description"

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Tue, 21 Jun 2011 19:49:49 +0400

festvox-ru (0.5-3) unstable; urgency=low

  * Bump up Standards-Version
  * Added "DM-Upload-Allowed: yes" control field
  * Switch to dpkg-source 3.0 (quilt) format

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Fri, 04 Mar 2011 14:49:53 +0300

festvox-ru (0.5-2) unstable; urgency=low

  * Bump Standards-Version to 3.8.4.
  * Change minimal festival requirements (See bug #545737).

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Wed, 19 May 2010 21:45:09 +0400

festvox-ru (0.5-1) unstable; urgency=low

  [ Sergey B Kirpichev ]
  * New Upstream Version.
  * debian/rules modified for Bzip2-archived sources.

  [ Dmitry E. Oboukhov ]
  * Fixed description, copyright, few lintian warnings.

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Sun, 22 Feb 2009 12:57:16 +0300

festvox-ru (0.4-2) unstable; urgency=low

  [ Dmitry E. Oboukhov ]
  * Git-repo has been created on git.debian.org.
  * Added VCS-* records to debian/control.

  [ Sergey B Kirpichev ]
  * Point to english Homepage (Russian stuff goes to README.Debian).
  * Document (README.Debian) --language russian option for festival (Bug #516262).

 -- Sergey B Kirpichev <skirpichev@gmail.com>  Fri, 20 Feb 2009 20:26:40 +0300

festvox-ru (0.4-1) unstable; urgency=low

  * Initial release. (Closes: #513396)

 -- Dmitry E. Oboukhov <unera@debian.org>  Tue, 26 Aug 2008 18:32:09 +0400
